# EP3 - OO 2019/2

**Aluno**: Josué Bonfim

**Matrícula**: 16/0032598

## Aplicação - Delay.com

Se trata de um clone do site [Reverb.com](https://reverb.com). É um site de compra e venda de instrumentos musicais usados. 

## Uso

Ao entrar no site, você poderá encontrar os instrumentos disponiveis para compra. Para comprar é necessário fazer o login. Se o usuário não tiver conta, deverá realizar o cadastro.

### Cadastro

É necessário preencher os campos *nome*, *e-mail*, *senha* e *confirmação de senha* para realizar o cadastro. A senha deverá ter no mínimo 6 caracteres.

### Login

Para realizar o login, é necessário preencer os campos *e-mail* e *senha*

### Cadastro de instrumentos

Para cadastrar um instrumento, o usuário deverá estar logado e clicar no botão venda na *homepage*. O usuário devera preencher os campos pedidos e colocar uma imagem. 

### Compra

Para comprar um instrumento, é necessário clicar no instrumento desejado e após clicar em adicionar ao carrinho. Para remover o instrumento do carrinho, basta clicar em remover do carrinho. Para excluir o carrinho, basta clicar em excluir carrinho. 

#### Rodando a aplicação.

É necessário dar um `bundle`. Se estiver rodando no MacOS X, é necessário instalar algumas `gems` manualmente. Após, é necessário dar o comando `rails db:migrate` e então, a aplicação pode ser rodada no navegador através [deste](https://localhost:3000) endereço
