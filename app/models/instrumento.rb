class Instrumento < ApplicationRecord
  before_destroy :not_reference_by_any_line_item
  belongs_to :user, optional: true
  has_many :line_items

  mount_uploader :imagem, ImagemUploader
  serialize :imagem, JSON # If you use SQLite, add this line

  validates :titulo, :marca, :preco, :modelo, presence: true
  validates :descricao, length: { maximum: 1000, too_long: "%{count} characters is the maximum aloud. "}
  validates :titulo, length: { maximum: 140, too_long: "%{count} characters is the maximum aloud. "}
  validates :preco, length: { maximum: 7 }

  MARCA = %w{ Fender Gibson Epiphone ESP Martin Dean Taylor Jackson PRS  Ibanez Charvel Washburn }
  CONDICAO = %w{ Novo Excelente Usado Péssimo }

  private
  def not_reference_by_any_line_item
    unless line_items.empty?
    	errors.add(:base, "Line items present")
	   throw :abort
    end
  end  
end
