json.extract! instrumento, :id, :marca, :modelo, :descricao, :condicao, :titulo, :preco, :created_at, :updated_at
json.url instrumento_url(instrumento, format: :json)
